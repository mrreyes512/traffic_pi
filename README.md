# PiTraffic
PiTraffic- Educational Traffic Light Add-on Shield HAT for Raspberry Pi

<img src="https://cdn.shopify.com/s/files/1/1217/2104/products/PiTraffic_720_660_1024x1024.png?v=1528203007" width="300">

1. Open Terminal and download the code by writing: 
   ```
   git clone https://github.com/sbcshop/PiTraffic.git
   ```

2. Your code will be downloaded to '/home/pi' directory. Use 'ls' command to check the list of directories

3. 'TrafficTest.py' is example code. Run this file to test and play with PiTraffic
